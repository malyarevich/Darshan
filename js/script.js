var s1 = Swiped.init({
    query: 'li>.swipe-right',
    list: false,
    left: 5,
    right: 0
});
var s2 = Swiped.init({
    query: 'li>.swipe-left-right',
    list: false,
    left: 5,
    right: 5
});
jQuery(document).on('touchstart', 'li>.swipe-right', function() {
    s1.open;
});
jQuery(document).on('touchstart', 'li>.swipe-left-right', function() {
    s2.open;
});

var onOpenfunction = function() {
    var clickUndo = false;
    var dir = this.dir;
    var offset, invert_offser;
    //jQuery(this.elem).css('transform', 'translate3d(0px, 0px, 0px)');
    if (dir === 1) {
        offset = '110%';
        invert_offser = '-110%';
    } else {
        offset = '-110%';
        invert_offser = '110%';
    }
    jQuery(this.elem).animate({left: offset});

    console.log(offset);
    jQuery('.patient-list__info--mirror',jQuery(this.elem).parent()).css('left',invert_offser).animate({left: 0});

    jQuery('.patient-list__info--mirror',jQuery(this.elem).parent()).click(function () {

        jQuery('.patient-list__info',jQuery(this).parent()).not('.patient-list__info--mirror').animate({left: 0});
        jQuery(this).animate({left: invert_offser});
        clickUndo = true;
        jQuery(this).off();
    });
    var currentElem = this.elem;
    var parentElem = jQuery(this.elem).parent();
    setTimeout(function() {
        if(!clickUndo) {
            jQuery(parentElem).hide();
            if(dir === 1){
                console.log(jQuery(currentElem).attr('linkRight'));
                window.location.replace(jQuery(currentElem).attr('linkRight'));
            } else {
                console.log(jQuery(currentElem).attr('linkLeft'));
                window.location.replace(jQuery(currentElem).attr('linkLeft'));
            }
        }
    }, 5000);
};

Swiped.init({
    query: 'ul.swipe-list>li>.swipe-left-right',
    onOpen: onOpenfunction
});
Swiped.init({
    query: 'ul.swipe-list>li>.swipe-right',
    onOpen: onOpenfunction
});
